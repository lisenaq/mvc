package org.lisen.mvc.framework;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActionModel implements Serializable {

	private static final long serialVersionUID = 6698313347150868319L;

	private static Pattern pattern = Pattern.compile("^/.+$");

	private String path;
	private String type;
	private Map<String, ForwardModel> forwardModels = new HashMap<String, ForwardModel>();

	public ActionModel() {
		super();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.checkPath(path);
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void put(ForwardModel forwardModel) {
		if (this.forwardModels.containsKey(forwardModel.getName())) {
			throw new RuntimeException(
					"ActionModel[" + path + "]/forwardModel.name[" + forwardModel.getName() + "]已存在");
		}
		this.forwardModels.put(forwardModel.getName(), forwardModel);
	}

	public ForwardModel find(String forwardName) {
		if (!this.forwardModels.containsKey(forwardName)) {
			throw new RuntimeException("ActionModel[" + path + "]/forwardModel.name[" + forwardName + "]不存在");
		}
		return this.forwardModels.get(forwardName);
	}

	private void checkPath(String path) {
		Matcher matcher = pattern.matcher(path);
		boolean b = matcher.matches();
		if (!b) {
			throw new RuntimeException("ActionModel.path[" + path + "]必须以/开头");
		}
	}
}
