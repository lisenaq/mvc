package org.lisen.mvc.framework;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForwardModel implements Serializable {

	private static final long serialVersionUID = 4907025244685971932L;

	private static Pattern pattern = Pattern.compile("^/.+$");

	private String name;
	private String path;
	private boolean redirect;

	public ForwardModel() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.checkPath(path);
		this.path = path;
	}

	public boolean isRedirect() {
		return redirect;
	}

	public void setRedirect(boolean redirect) {
		this.redirect = redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = Boolean.parseBoolean(redirect);
	}

	private void checkPath(String path) {
		Matcher matcher = pattern.matcher(path);
		boolean b = matcher.matches();
		if (!b) {
			throw new RuntimeException("ForwardModel.path[" + path + "]必须以/开头");
		}
	}

	@Override
	public String toString() {
		return "ForwardModel [name=" + name + ", path=" + path + ", redirect=" + redirect + "]";
	}

}
