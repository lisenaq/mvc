package org.lisen.mvc.framework;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 每个子控制器必须实现该接口，负责处理中央处理器分配的请求
 * @author Administrator
 */
public interface Action {
	
	/**
	 * 处理请求
	 * @param request  请求
	 * @param response 响应
	 * @return String 返回转发或重定向的jsp页面名称
	 */
	String exeute(HttpServletRequest request, HttpServletResponse response);

}
