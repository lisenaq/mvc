package org.lisen.mvc.framework;

/**
 * 对于需要处理请求参数的Action可以通过实现该接口获取请求参数的
 * 处理能力，中央控制器将会使用该接口来获取Model对象，并统一处理
 * 参数
 * @author Administrator
 */
public interface ModelDrive {
	
	Object getModel();

}
