package org.lisen.mvc.framework;

import java.lang.reflect.Method;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractDispatchAction implements Action {

	@Override
	public final String exeute(HttpServletRequest request, HttpServletResponse response) {
		
		//getStudents
		String methodName = request.getParameter("methodName");
		Class<? extends AbstractDispatchAction> clazz = this.getClass();
		try {
			Method method = clazz.getDeclaredMethod(
					methodName, 
					HttpServletRequest.class,
					HttpServletResponse.class);
			return (String)method.invoke(this, request,response);
		} catch (Exception e) {
			throw new RuntimeException("在调用Action中的["+methodName+"]方法是异常", e);
		} 
	}

}
