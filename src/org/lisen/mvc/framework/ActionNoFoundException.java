package org.lisen.mvc.framework;

/**
 * 如果根据请求未找到匹配的Action则是抛出该异常
 * @author Administrator
 */
public class ActionNoFoundException extends RuntimeException {
	
	public ActionNoFoundException() {
		super();
	}
	
	public ActionNoFoundException(String msg) {
		super(msg);
	}
	
	public ActionNoFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
