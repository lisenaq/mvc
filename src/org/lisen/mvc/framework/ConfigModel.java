package org.lisen.mvc.framework;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ConfigModel implements Serializable {

	private static final long serialVersionUID = 4887440087936031037L;

	private Map<String, ActionModel> actionModels = new HashMap<String, ActionModel>();

	public ConfigModel() {
		super();
	}

	public void put(ActionModel actionModel) {
		if (this.actionModels.containsKey(actionModel.getPath())) {
			throw new RuntimeException("ActionModel[" + actionModel.getPath() + "]已存在");
		}
		this.actionModels.put(actionModel.getPath(), actionModel);
	}

	public ActionModel find(String actionPath) {
		if (!this.actionModels.containsKey(actionPath)) {
			throw new ActionNoFoundException("ActionModel[" + actionPath + "]不存在");
		}
		return this.actionModels.get(actionPath);
	}
}
