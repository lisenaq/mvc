package org.lisen.mvc.framework;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.lisen.mvc.model.Student;

public class BeanUtilsDemo {
	
	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
		Map<String, String[]> param = new HashMap<String, String[]>();
		
		//如果不配置，则Student的age字段则为0，配置后为null
		ConvertUtils.register(new IntegerConverter(null), Integer.class);
		
		param.put("age", new String[]{""});
		param.put("sname", new String[]{"zss"});
		param.put("sid", new String[]{"1002"});
		
		Student student = new Student();
		BeanUtils.populate(student, param);
		
		System.out.println(student);
		
	}

}
