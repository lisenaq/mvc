package org.lisen.mvc.tag;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.beanutils.BeanUtils;

import com.mysql.jdbc.StreamingNotifiable;
import com.mysql.jdbc.StringUtils;

public class SelectTag extends BodyTagSupport {
	
	private String id;
	
	private String name;
	
	private List<?> items;
	
	private String value;
	
	private String text;
	
	private String cssClass;
	
	private String cssStyle;
	
	private String selectValue;
	
	private String onChange;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<?> getItems() {
		return items;
	}

	public void setItems(List<?> items) {
		this.items = items;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getCssStyle() {
		return cssStyle;
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}

	public String getSelectValue() {
		return selectValue;
	}

	public void setSelectValue(String selectValue) {
		this.selectValue = selectValue;
	}

	public String getOnChange() {
		return onChange;
	}

	public void setOnChange(String onChange) {
		this.onChange = onChange;
	}
	
	@Override
	public int doStartTag() {
		
		JspWriter out = this.pageContext.getOut();
		
		try {
			try {
				out.println(buildHTML());
			} catch (IllegalAccessException 
					| InvocationTargetException 
					| NoSuchMethodException e) {
				
				e.printStackTrace();
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		return SKIP_BODY;
	}
	
	private String buildHTML() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		
		StringBuilder html = new StringBuilder();
		html.append("<select id='" + this.id + "' name='"+this.name+"'");
		
		if(!StringUtils.isNullOrEmpty(this.cssClass)) {
			html.append(" class='"+this.cssClass+"'");
		}
		if(!StringUtils.isNullOrEmpty(this.cssStyle)) {
			html.append(" style='"+this.cssStyle+"'");
		}
		if(!StringUtils.isNullOrEmpty(this.onChange)) {
			html.append(" onchange='"+this.onChange+"'");
		}
		html.append(">");
		
		for(Object obj: this.items) {
			String val = BeanUtils.getProperty(obj, this.value);
			String txt = BeanUtils.getProperty(obj, this.text);
			
			if(!StringUtils.isNullOrEmpty(this.selectValue) 
					&& val.equals(this.selectValue)) {
				html.append("<option value='"+val+"' selected>" + txt + "</option>");
			} else {
				html.append("<option value='"+val+"'>" + txt + "</option>");
			}
		}
		
		html.append("</select>");
	
		return html.toString();
	}

}
