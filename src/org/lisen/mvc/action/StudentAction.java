package org.lisen.mvc.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lisen.mvc.framework.AbstractDispatchAction;
import org.lisen.mvc.framework.ModelDrive;
import org.lisen.mvc.model.Student;

public class StudentAction extends AbstractDispatchAction implements ModelDrive {
	
	private Student student = new Student();
	
	@Override
	public Object getModel() {
		return student;
	}

	/*@Override
	public String exeute(HttpServletRequest request, HttpServletResponse response) {		
		System.out.println("StudentAction = " + student);	
		return "students";
	}*/
	
	public String getStudents(HttpServletRequest request, HttpServletResponse response) {	
		System.out.println("getStudents");
		System.out.println("StudentAction = " + student);
		return "students";
	}
	
	public String addStudent(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("addStudent");
		System.out.println("add student = " + student);
		return "students";
	}

}
