package org.lisen.mvc.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 在数据表中的列明与实体对象中的属性名不一致时，
 * 可以使用该注解来指定列名。如果一致则不需要指定
 * 
 * @author Administrator
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
	String value();
}
