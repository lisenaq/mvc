package org.lisen.mvc.util;

@Table("test")
public class TestOrm {
	
	@Column("t_id")
	@AutoIncrement
	@Key
	private Integer tId;
	
	@Column("t_name")
	private String tName;
	
	@Column("ta_type")
	private String taType;
	
	private String remark;

	public Integer gettId() {
		return tId;
	}

	public void settId(Integer tId) {
		this.tId = tId;
	}

	public String gettName() {
		return tName;
	}

	public void settName(String tName) {
		this.tName = tName;
	}


	public String getTaType() {
		return taType;
	}


	public void setTaType(String taType) {
		this.taType = taType;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	@Override
	public String toString() {
		return "TestOrm [tId=" + tId + ", tName=" + tName + ", taType=" + taType + ", remark=" + remark + "]";
	}

	
}
