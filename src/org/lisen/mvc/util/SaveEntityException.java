package org.lisen.mvc.util;

/**
 * 在将实体保存到对应的数据表中时有可能发生该异常
 * @author Administrator
 *
 */
public class SaveEntityException extends RuntimeException {

	public SaveEntityException() {
		super();
	}

	public SaveEntityException(String message, Throwable cause) {
		super(message, cause);
	}

	public SaveEntityException(String message) {
		super(message);
	}

	public SaveEntityException(Throwable cause) {
		super(cause);
	}

}
