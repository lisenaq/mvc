package org.lisen.mvc.util;

public class QueryRecordException extends RuntimeException {

	public QueryRecordException() {
		super();
	}

	public QueryRecordException(String message, Throwable cause) {
		super(message, cause);
	}

	public QueryRecordException(String message) {
		super(message);
	}

	public QueryRecordException(Throwable cause) {
		super(cause);
	}

}
