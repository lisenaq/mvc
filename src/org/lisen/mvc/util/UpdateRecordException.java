package org.lisen.mvc.util;

/**
 * 更新数据库记录时有可能发生该异常
 * @author Administrator
 *
 */
public class UpdateRecordException extends RuntimeException {

	public UpdateRecordException() {
		super();
	}

	public UpdateRecordException(String message, Throwable cause) {
		super(message, cause);
	}

	public UpdateRecordException(String message) {
		super(message);
	}

	public UpdateRecordException(Throwable cause) {
		super(cause);
	}

}
