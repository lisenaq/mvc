package org.lisen.mvc.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 建议使用DbTemplate封装类对数据库进行操作
 * @deprecated
 * @author Administrator
 * @see org.lisen.mvc.util.DaoTemplate.java
 */
public final class DaoTemplate {
	
	private DaoTemplate() {}
	
	//接口结果集转换处理接口
	@FunctionalInterface
	public static interface IORMConvert <E> {
		List<E> convert(ResultSet rs) throws SQLException;
	}
	
	/**
	 * 执行分页查询
	 * 
	 * @param sql 需要执行的sql语句
	 * @param args 查询条件
	 * @param pageBean 分页对象
	 * @param ormConverter ORM转换处理器
	 * @return
	 */
	public static <E> List<E> query(
			String sql, 
			Object[] args, 
			PageBean pageBean, 
			IORMConvert<E> ormConverter) {
		
		List<E> datas = new ArrayList<>();
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		//统计总记录数
		if(pageBean != null && pageBean.isPagination()) {
			try {
				con = DBUtil.getConection();
				
				String countsql = "select count(*) from (" + sql + ") t";
				ps = con.prepareStatement(countsql);
				
				//设置查询参数
				if(args != null && args.length > 0) {
					int i = 1;
					for(Object arg:  args) {
						ps.setObject(i, arg);
						i++;
					}
				}
				
				rs = ps.executeQuery();
				
				while(rs.next()) {
					pageBean.setTotal(rs.getInt(1));
				}
				
			} catch (SQLException e) {
				DBUtil.closeDB(rs, ps, con);
				throw new RuntimeException("执行统计记录数sql异常", e);
			} finally {
				DBUtil.closeDB(rs, ps);
			}
			
			//如果没有符合条件的记录则返回空结果集
			if(pageBean.getTotal() == 0) {
				return datas;
			}
		}
		
		try {
			con = con == null ? DBUtil.getConection() : con;
			
			//需要分页则需要封装sql
			if(pageBean != null && pageBean.isPagination()) {
				sql += "limit " 
						+ pageBean.getStartIndex() 
						+ ", " 
						+ pageBean.getRows();
			}
			ps = con.prepareStatement(sql);
			
			//设置查询参数
			int i = 1;
			for(Object arg:  args) {
				ps.setObject(i, arg);
				i++;
			}
			
			rs = ps.executeQuery();
			
			//转换结果集
			datas = ormConverter.convert(rs);
			
		} catch(SQLException e) {
			throw new RuntimeException("执行sql查询异常", e);
		} finally {
			DBUtil.closeDB(rs, ps, con);
		}
		
		return datas;
	}
	
	
	public static <E> List<E> query(
			String sql, 
			Object[] args, 
			IORMConvert<E> ormConverter) {
		
		return query(sql, args, null, ormConverter);
	}
	
	
	public static <E> List<E> query(
			String sql, 
			IORMConvert<E> ormConverter) {
		return query(sql, null, null, ormConverter);
	}
	
	
	public static <E> List<E> query(
			String sql, 
			PageBean pageBean,
			IORMConvert<E> ormConverter) {
		return query(sql, null, pageBean, ormConverter);
	}
	
	
	/**
	 * 新增，更新，或删除记录
	 * @param sql 更新sql语句
	 * @param args 参数数组
	 * @return int 影响的行数
	 */
	public static <E> int update(String sql, Object[] args) {
		
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DBUtil.getConection();
			ps = con.prepareStatement(sql);
			
			int i = 1;
			for(Object arg: args) {
				ps.setObject(i, arg);
				i++;
			}
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new UpdateRecordException("执行："+ sql, e);
		} finally {
			DBUtil.closeDB(null, ps, con);
		}
	}

}
