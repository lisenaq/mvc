package org.lisen.mvc.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * @author Administrator
 * @create 2020-05-0811:43
 */
public final class DBUtil {
	
    private static String DRIVER_NAME;

    private static String DB_URL;

    private static String DB_USER;

    private static String DB_PASSWORD;


    private DBUtil() {
    }


    static {
        try {
        	InputStream in = DBUtil.class.getResourceAsStream("/jdbc.properties");
        	Properties properties = new Properties();
			properties.load(in);
			
			DRIVER_NAME = properties.getProperty("driver.name");
			DB_URL = properties.getProperty("db.url");
			DB_USER = properties.getProperty("db.user");
			DB_PASSWORD = properties.getProperty("db.password");
			
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }


    public static Connection getConection() throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        return connection;
    }


    public static void closeDB(ResultSet rs, Statement ps, Connection con) {

        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }

            if (ps != null && !ps.isClosed()) {
                ps.close();
            }

            if(con != null && !con.isClosed()) {
                con.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
    public static void closeDB(ResultSet rs, Statement ps) {

        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }

            if (ps != null && !ps.isClosed()) {
                ps.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
    public static void main(String[] args) throws SQLException {
    	Connection conection = DBUtil.getConection();
    	DBUtil.closeDB(null, null, conection);
    	System.out.println("------- �ɹ� -------------");
    }

}
